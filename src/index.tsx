import * as yup from 'yup';
import React, {useEffect} from 'react';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {Pressable, ScrollView, StyleSheet, Text, View} from 'react-native';
import CustomTextField from './components/CustomTextField';
import {Root} from './types/root';

function DynamicComponent(props: Root): JSX.Element {
  const schema = yup.object().shape(props.yup_schema);

  const {
    register,
    setValue,
    handleSubmit,
    formState: {errors},
    reset,
    getValues,
    watch,
  } = useForm({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    props.fields.map(field => {
      watch(field.key);
      register(field.key);
    });
  }, [register, watch]);

  const submit = data => {
    console.log('data', data);
  };

  return (
    <ScrollView contentContainerStyle={{}}>
      <View>
        <Text style={{marginBottom: 8}}>{props.title}</Text>
        {props.fields.map(field => {
          if (field.component === 'input') {
            return (
              <View style={{marginBottom: 8}}>
                <CustomTextField
                  value={getValues(field.key)}
                  placeholder={field.placeholder}
                  error={errors[field.key]}
                  onChangeText={text => setValue(field.key, text)}
                />
              </View>
            );
          }
        })}
      </View>
      <Pressable
        style={{
          backgroundColor: '#ff8',
          height: 60,
          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={handleSubmit(submit)}>
        <Text>submit</Text>
      </Pressable>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default DynamicComponent;
