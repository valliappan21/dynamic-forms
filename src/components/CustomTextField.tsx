import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TextInput, useColorScheme, View} from 'react-native';

const CustomTextField = (
  {
    error,
    label,
    leadingAccessory,
    trailingAccessory,
    customStyle,
    ...inputProps
  },
  ref,
) => {
  const [errors, setErrors] = useState(error);
  useEffect(() => {
    setErrors(error);
  }, [error]);
  return (
    <View style={{marginTop: 2, marginBottom: 8}}>
      <View style={[styles.root, errors ? styles.errorStyle : null]}>
        {leadingAccessory ? (
          <View style={{paddingStart: 8}}>{leadingAccessory}</View>
        ) : null}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            flex: 1,
            alignItems: 'center',
            backgroundColor: useColorScheme() === 'light' ? '#fff' : '#000',
          }}>
          <TextInput
            ref={ref}
            forwardedRef
            {...inputProps}
            style={[
              styles.mainContainer,
              {color: useColorScheme() === 'light' ? '#000' : '#fff'},
              customStyle || null,
            ]}
            onFocus={() => {
              if (errors != null) {
                console.log('cleard');
                setErrors(null);
              }
            }}
          />
          {trailingAccessory ? (
            <View style={{paddingRight: 8}}>{trailingAccessory}</View>
          ) : null}
        </View>
      </View>

      {errors && <Text style={styles.error}>{errors.message}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    color: '#a2a2a2',
    textTransform: 'capitalize',
  },
  root: {
    height: 48,
    // backgroundColor: '#f5f5f5',
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#1c64f2',
    borderWidth: 1,
    paddingHorizontal: 4,
  },
  mainContainer: {
    height: 41,
    flex: 1,
    paddingHorizontal: 8,
    backgroundColor: 'transparent',
  },
  error: {
    color: '#FF0000',
    marginBottom: 8,
    marginStart: 8,
  },
  errorStyle: {
    borderColor: '#FF0000',
  },
});

export default React.forwardRef(CustomTextField);
