
export interface Root {
  title: string;
  slug: string;
  api: Api;
  yup_schema: any;
  fields: Field[];
}

export interface Api {
  url: string;
}

export interface YupSchema {}

export interface Field {
  reference?: string;
  component:
    | 'date_picker'
    | 'date_time_picker'
    | 'picker'
    | 'input'
    | 'chip'
    | 'radio'
    | 'filerpicker'
    | 'slider'
    | 'checkbox'
    | 'avatar';
  placeholder: string;
  lablel: string;
  key: string;
  value: string;
  icon: string;
  default_value: string;
  options: Option[];
  api: Api;
  visible_on: any[];
  input_format: 'number' | 'tel' | 'alphabets';
  content_type: string;
  container_style?: 'jpg' | 'png' | 'pdf' | 'docx';
  is_mutlipe: boolean;
  min_value: string;
  max_value: string;
  toggle: string;
}

export interface Option {
  id: string;
  label: string;
  default: boolean;
}
