const fs = require("fs");
var formType = "medical_form";
const fileName = `${formType}.json`;
var masterData = {};
var formConfiguration = {};
fs.readFile("master.json", "utf-8", (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
  masterData = JSON.parse(data);
});

if (fs.existsSync(fileName)) {
  fs.readFile(fileName, "utf8", (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
    const formData = JSON.parse(data);
    const fields = formData["fields"];
    var outputFields = [];
    for (var i = 0; i < fields.length; i++) {
      var field = fields[i];
      if ("reference" in field) {
        field = {
          ...masterData[field["reference"]],
          ...field,
        };
      }
      outputFields[i] = field;
    }
    formConfiguration = {
      ...formData,
      fields: outputFields,
    };
    console.log(formConfiguration);
  });
}
