/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import * as yup from 'yup';
import DynamicComponent from './src';
import {View} from 'react-native';
import {Root} from './src/types/root';

function App(props: Root): JSX.Element {
  return (
    <View style={{padding: 16, marginTop: 64}}>
      <DynamicComponent
        api={{
          url: '/api/medical',
        }}
        title="Medical"
        slug="slug"
        yup_schema={{
          first_name: yup.string().required('required'),
        }}
        fields={[
          {
            reference: 'input',
            component: 'input',
            placeholder: 'enter first name',
            lablel: 'first name',
            key: 'first_name',
            value: 'first_name',
            icon: '',
            default_value: 'first_name',
            options: [],
            api: {
              url: '',
            },
            visible_on: [],
            input_format: 'alphabets',
            content_type: '',
            is_mutlipe: false,
            min_value: '',
            max_value: '',
            toggle: '',
          },
        ]}
      />
    </View>
  );
}

export default App;
